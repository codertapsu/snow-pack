// Snowpack Configuration File
// See all supported options: https://www.snowpack.dev/reference/configuration

/** @type {import("snowpack").SnowpackUserConfig } */
module.exports = {
  exclude: [
    "**/node_modules/**/*"
  ],
  mount: {
    /* ... */
  },
  plugins: [
    /* ... */
    ["@snowpack/plugin-sass", {
      compilerOptions: {
        style: "compressed",
        sourceMap: false,
        loadPath: []
      }
    }],
    "@snowpack/plugin-typescript" 
  ],
  packageOptions: {
    polyfillNode: true
    /* ... */
  },
  devOptions: {
    /* ... */
  },
  buildOptions: {
    out: "dist",
    clean: true,
    minify: false,
    /* ... */
  },
};
